package logic;

import java.util.List;

/**
 * Excel像素画生成器.
 * @author Rex
 *
 */
public class PicToXlsx {
	
	public static void main(String[] args) throws Exception {

		long a = System.currentTimeMillis();
		args = checkArgs(args);
		// list的容量为图片高度（行数），rgb数组的长度为图片宽度（列数）
		List<int[]> load = LoadPic.load(args[0]);

		// 填入到Excel
		CreateXlsx.create(load, args[1], args[2]);
		long b = System.currentTimeMillis();
		System.out.println((double) (b - a) / 1000 / 60);
	}
	
	/**
	 * 参数检查.
	 * @param args
	 * @throws Exception
	 */
	private static String[] checkArgs(final String[] args) throws Exception {
		String[] infos = new String[3];
		
		int len = args.length;
		if (!(len == 2 || len == 3)) {
			throw new Exception("Incorrect number of parameters.");
		} else {
			// 检查第一个参数
			args[0] = args[0].toLowerCase().trim();
			if (!(args[0].endsWith(".jpg") || args[0].endsWith(".png") || args[0].endsWith(".gif")
					|| args[0].endsWith(".bmp") || args[0].endsWith(".jpeg") || args[0].endsWith(".jpe")
					|| args[0].endsWith(".jfif")))
				throw new Exception("Image format error.");

			// 检查第二个参数
			if (!(args[1] = args[1].toLowerCase().trim()).endsWith(".xlsx"))
				throw new Exception("The output file must be an xlsx file.");
			
			infos[0] = args[0];
			infos[1] = args[1];
			
			// 检查第三个参数
			if (len < 3)
				infos[2] = "12";
			else
				infos[2] = args[2].trim();
			
			return infos;
		}
	}
	
}