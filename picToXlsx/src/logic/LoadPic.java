package logic;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

public class LoadPic {

	/**
	 * 加载图片，获取像素点RGB信息.
	 * @param path
	 * @return
	 * @throws Exception
	 */
	static List<int[]> load(String path) throws Exception {
		System.out.println("Loading picture...");
		BufferedImage bi = ImageIO.read(new File(path));

		int width = bi.getWidth();
		int height = bi.getHeight();
		int minx = bi.getMinX();
		int miny = bi.getMinY();
		int len = width - minx;
		
		List<int[]> pic = new ArrayList<>(height - miny);

		// 按行遍历像素点
		for (int j = miny; j < height; j++) {
			int[] array = new int[len];
			int index = 0;
			for (int i = minx; i < width; i++) {
				array[index] = bi.getRGB(i, j);
				index++;
			}
			pic.add(array);
		}

		return pic;
	}
	
}